---
name: Design tokens
---

<todo issue="https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1569">Create working design tokens definition.</todo>

<todo issue="https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1570">Define starting point, and document purpose.</todo>
